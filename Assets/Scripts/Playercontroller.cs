using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Playercontroller : MonoBehaviour
{
    public int speed = 5;
    public int jumpForce = 800;
    private Rigidbody2D rb;
    private Animator _animator;
    public bool grounded;
    public Transform feet;
    public LayerMask groundLayer;
<<<<<<< HEAD
 private AudioSource _audio;
 public AudioClip shootSnd;
 public AudioClip jumpSnd;
 public AudioClip hurtsSnd;
=======
    private int level = 1;
    private AudioSource _audio;
    public AudioClip shootSnd;
    public AudioClip jumpSnd;
    public AudioClip hurtsSnd;
>>>>>>> ae4cde69918439085c49c2bd017c0c3627e3fdcb
    public GameObject bulletPrefab;
    public Image Health;
    public Text LivesText;
    public int Lives=3;
    public Transform PlayerPos;
    public SpriteRenderer sr;

   // Start is called before the first frame update
    void Start() {
        rb = GetComponent<Rigidbody2D>();//cached reference to player 1
        _animator = GetComponent<Animator>();
        _audio = GetComponent<AudioSource>();
    }


    // Update is called once per frame
    void Update()
    {
        float xSpeed = Input.GetAxisRaw("Horizontal") * speed;
        rb.velocity = new Vector2(xSpeed,rb.velocity.y);
        _animator.SetFloat("Speed", Mathf.Abs(xSpeed));

        /*if the character's facing the left
        & if character's moving left and facing right flip the scale
        and vice versa
        */
        // if (xSpeed < 0 && transform.localScale.x > 0 || xSpeed > 0 && transform.localScale.x < 0)
        // {
        //     transform.localScale *= new Vector2(-1, 1);
        // }

        if (xSpeed <0){
            sr.flipX=true;
        }
        else {
            sr.flipX=false;
        }
        
        grounded = Physics2D.OverlapCircle(feet.position, 0.5f, groundLayer);

        if (Input.GetButtonDown("Jump") && grounded)
        {
            rb.AddForce(new Vector2(0,jumpForce));
            _audio.PlayOneShot(jumpSnd);
        }
        _animator.SetBool("Grounded",grounded);
       
        
        MinusLives();
        CheckLoss();
    }
private void MinusLives(){
if (Health.fillAmount<=0)
{
    Lives--;
<<<<<<< HEAD
    LivesText.text="Lives:"+Lives;
_animator.SetBool("Dead",true);
       Revive();
}
}
private void Revive(){
 _animator.SetBool("Dead",false);
=======
    LivesText.text="Lives:"+" "+Lives;
>>>>>>> ae4cde69918439085c49c2bd017c0c3627e3fdcb
    Health.fillAmount=1;
 transform.position=new Vector3(PlayerPos.position.x,PlayerPos.position.y,PlayerPos.position.z);
}

    private void CheckLoss(){
     if (Lives==0)
        {
SceneManager.LoadScene("Gameover");
        }
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        //add condition for the enemy being dead
        if (other.gameObject.CompareTag("Door"))
        {
     Scene currentScene = SceneManager.GetActiveScene ();
     if (currentScene.name == "Level1")
     {
            SceneManager.LoadScene("Level2");
        }
         if (currentScene.name == "Level2")
     {
           
            SceneManager.LoadScene("Level3");
            
        }
         if (currentScene.name == "Level3")
     {
        
            SceneManager.LoadScene("Win");
        }
   }
}
}

