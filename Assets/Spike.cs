using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spike : MonoBehaviour

{
    public float Speed = 2.0f; 

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
transform.Translate(Vector2.down*Time.deltaTime*Speed);
    }

    private void OnCollisionEnter2D(Collision2D other) {
        if (other.gameObject.tag=="Floor")
        {
        Destroy(this.gameObject);
        }
        if (other.gameObject.tag=="Player"){
            
        }
        
    }
}
