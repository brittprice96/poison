﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void Bullet::Awake()
extern void Bullet_Awake_m2D77C2A3CF11F66E86FF074B8C4397C0E3DE2004 (void);
// 0x00000002 System.Void Bullet::DestroyObject()
extern void Bullet_DestroyObject_mD497B149D9DAE853A9964CE406E9E9E2F80841CA (void);
// 0x00000003 System.Collections.IEnumerator Bullet::DestroyByTime()
extern void Bullet_DestroyByTime_mC25B93ADD21719A6ABD13729A63D63D111911DBF (void);
// 0x00000004 System.Void Bullet::Update()
extern void Bullet_Update_mB82408CA535D8533168045E7C0321090448B596B (void);
// 0x00000005 System.Void Bullet::.ctor()
extern void Bullet__ctor_mC7D931FE508342F413FBA79525A4819D4114B3EC (void);
// 0x00000006 System.Void Bullet/<DestroyByTime>d__6::.ctor(System.Int32)
extern void U3CDestroyByTimeU3Ed__6__ctor_mEC93DA15D1FA5E2D1B7FD8A34D662BC22A19FB7F (void);
// 0x00000007 System.Void Bullet/<DestroyByTime>d__6::System.IDisposable.Dispose()
extern void U3CDestroyByTimeU3Ed__6_System_IDisposable_Dispose_mD1317F823067BC5BAF6E9CEF62C6C896D3FC29CE (void);
// 0x00000008 System.Boolean Bullet/<DestroyByTime>d__6::MoveNext()
extern void U3CDestroyByTimeU3Ed__6_MoveNext_m8734E080EDDF923C4C15139EDF1443EA3857B34D (void);
// 0x00000009 System.Object Bullet/<DestroyByTime>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDestroyByTimeU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6F3C7B05D4B6CE41AED74D3C950806F2840848FD (void);
// 0x0000000A System.Void Bullet/<DestroyByTime>d__6::System.Collections.IEnumerator.Reset()
extern void U3CDestroyByTimeU3Ed__6_System_Collections_IEnumerator_Reset_m2B3734B45DE23CB8104BD7F51093F84D83DA154F (void);
// 0x0000000B System.Object Bullet/<DestroyByTime>d__6::System.Collections.IEnumerator.get_Current()
extern void U3CDestroyByTimeU3Ed__6_System_Collections_IEnumerator_get_Current_m4A2CF5FAC4571846D6609065CF7F7AC05E2CF550 (void);
// 0x0000000C System.Void Health::ReduceHealth()
extern void Health_ReduceHealth_mC0C636826BF05A7775DEB32BABE0EE19D6F733B1 (void);
// 0x0000000D System.Void Health::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void Health_OnTriggerEnter2D_m2B23A3F79959C045A0D8DAE8A859070AC2BEA372 (void);
// 0x0000000E System.Void Health::.ctor()
extern void Health__ctor_mD50C73D87211EAE260B38B2936F01722E31B9416 (void);
// 0x0000000F System.Void MenuManager1::StartButton()
extern void MenuManager1_StartButton_m5909896F75FCE6CDC851AD7C898B04FFFF15D361 (void);
// 0x00000010 System.Void MenuManager1::QuitButton()
extern void MenuManager1_QuitButton_mEF5920CD537C0FA1210043A827D787D2AC8452CE (void);
// 0x00000011 System.Void MenuManager1::.ctor()
extern void MenuManager1__ctor_mB7EB9D75B78C4642346DC7ADCD12FC0BA5818B9A (void);
// 0x00000012 System.Void Player2controller::Start()
extern void Player2controller_Start_mA97F60A2109D1D65633E188C6A67354A65ECA5EA (void);
// 0x00000013 System.Void Player2controller::Update()
extern void Player2controller_Update_m848B035D5AE7D9AD2FB1262F86819676F8C15197 (void);
// 0x00000014 System.Void Player2controller::.ctor()
extern void Player2controller__ctor_mDF09A6FD17633D29E59A4FE4411C4B8D815F77EB (void);
// 0x00000015 System.Void Playercontroller::Start()
extern void Playercontroller_Start_m6BB977C79410D4A8259DBCB2029A653907AF7E24 (void);
// 0x00000016 System.Void Playercontroller::Update()
extern void Playercontroller_Update_mF31404150730A4C36C68CA91C66CF5E32F6B00B3 (void);
// 0x00000017 System.Void Playercontroller::OnCollisionEnter2D(UnityEngine.Collision2D)
extern void Playercontroller_OnCollisionEnter2D_mC85801E167C053F5C8B8907C8CA74B60118069A9 (void);
// 0x00000018 System.Void Playercontroller::.ctor()
extern void Playercontroller__ctor_mBB34D0CEEF42FFDAA0E923AE3B20F651590F80B6 (void);
// 0x00000019 System.Void Wizardcontroller::Start()
extern void Wizardcontroller_Start_m9FB8CBE8D7AA1F39860B1A03D733D956EB7FE50B (void);
// 0x0000001A System.Void Wizardcontroller::Update()
extern void Wizardcontroller_Update_m134565827F126FC3252189B89F9F8E9ED2E4F7E9 (void);
// 0x0000001B System.Void Wizardcontroller::Firebullet()
extern void Wizardcontroller_Firebullet_m81BC0069E8C95DBDE2DE6052C5A2863022DF20B5 (void);
// 0x0000001C System.Void Wizardcontroller::.ctor()
extern void Wizardcontroller__ctor_mDBB6CCA826D03A12CD271DB95BACD6571943C9B2 (void);
static Il2CppMethodPointer s_methodPointers[28] = 
{
	Bullet_Awake_m2D77C2A3CF11F66E86FF074B8C4397C0E3DE2004,
	Bullet_DestroyObject_mD497B149D9DAE853A9964CE406E9E9E2F80841CA,
	Bullet_DestroyByTime_mC25B93ADD21719A6ABD13729A63D63D111911DBF,
	Bullet_Update_mB82408CA535D8533168045E7C0321090448B596B,
	Bullet__ctor_mC7D931FE508342F413FBA79525A4819D4114B3EC,
	U3CDestroyByTimeU3Ed__6__ctor_mEC93DA15D1FA5E2D1B7FD8A34D662BC22A19FB7F,
	U3CDestroyByTimeU3Ed__6_System_IDisposable_Dispose_mD1317F823067BC5BAF6E9CEF62C6C896D3FC29CE,
	U3CDestroyByTimeU3Ed__6_MoveNext_m8734E080EDDF923C4C15139EDF1443EA3857B34D,
	U3CDestroyByTimeU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6F3C7B05D4B6CE41AED74D3C950806F2840848FD,
	U3CDestroyByTimeU3Ed__6_System_Collections_IEnumerator_Reset_m2B3734B45DE23CB8104BD7F51093F84D83DA154F,
	U3CDestroyByTimeU3Ed__6_System_Collections_IEnumerator_get_Current_m4A2CF5FAC4571846D6609065CF7F7AC05E2CF550,
	Health_ReduceHealth_mC0C636826BF05A7775DEB32BABE0EE19D6F733B1,
	Health_OnTriggerEnter2D_m2B23A3F79959C045A0D8DAE8A859070AC2BEA372,
	Health__ctor_mD50C73D87211EAE260B38B2936F01722E31B9416,
	MenuManager1_StartButton_m5909896F75FCE6CDC851AD7C898B04FFFF15D361,
	MenuManager1_QuitButton_mEF5920CD537C0FA1210043A827D787D2AC8452CE,
	MenuManager1__ctor_mB7EB9D75B78C4642346DC7ADCD12FC0BA5818B9A,
	Player2controller_Start_mA97F60A2109D1D65633E188C6A67354A65ECA5EA,
	Player2controller_Update_m848B035D5AE7D9AD2FB1262F86819676F8C15197,
	Player2controller__ctor_mDF09A6FD17633D29E59A4FE4411C4B8D815F77EB,
	Playercontroller_Start_m6BB977C79410D4A8259DBCB2029A653907AF7E24,
	Playercontroller_Update_mF31404150730A4C36C68CA91C66CF5E32F6B00B3,
	Playercontroller_OnCollisionEnter2D_mC85801E167C053F5C8B8907C8CA74B60118069A9,
	Playercontroller__ctor_mBB34D0CEEF42FFDAA0E923AE3B20F651590F80B6,
	Wizardcontroller_Start_m9FB8CBE8D7AA1F39860B1A03D733D956EB7FE50B,
	Wizardcontroller_Update_m134565827F126FC3252189B89F9F8E9ED2E4F7E9,
	Wizardcontroller_Firebullet_m81BC0069E8C95DBDE2DE6052C5A2863022DF20B5,
	Wizardcontroller__ctor_mDBB6CCA826D03A12CD271DB95BACD6571943C9B2,
};
static const int32_t s_InvokerIndices[28] = 
{
	1534,
	1534,
	1498,
	1534,
	1534,
	1283,
	1534,
	1520,
	1498,
	1534,
	1498,
	1534,
	1295,
	1534,
	1534,
	1534,
	1534,
	1534,
	1534,
	1534,
	1534,
	1534,
	1295,
	1534,
	1534,
	1534,
	1534,
	1534,
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	28,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_AssemblyU2DCSharp_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
